import * as React from 'react';
import { Route, Switch } from 'react-router';
import { App as TodoApp } from 'app/containers/App';
import User from 'app/containers/User';
import { hot } from 'react-hot-loader';

export const App = hot(module)(() => (
  <Switch>
    <Route exact={true} path="/user" component={TodoApp} />
    <Route path="/" component={User} />
  </Switch>
));
