/** UserMVC model definitions **/

export interface UserModel {
  id: number;
  name: string;
  login: string;
  age: number;
  verified: boolean;
}
