import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RouteComponentProps } from 'react-router';
import { UserActions } from '../../actions';
import { RootState } from '../../reducers';
import { omit } from '../../utils';
import UserList from '../../components/UserList';

export namespace User {
  export interface Props extends RouteComponentProps<void> {
    users: RootState.UserState;
    actions: UserActions;
}
}

export default connect(
  (state: RootState): Pick<User.Props, 'users'> => {

    return { users: state.users };
  },
  (dispatch: Dispatch): Pick<User.Props, 'actions'> => ({
    actions: bindActionCreators(omit(UserActions, 'Type'), dispatch)
  })
)(UserList);

