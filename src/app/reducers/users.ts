import { handleActions } from 'redux-actions';
import { RootState } from './state';
import { UserActions } from '../actions/users';
import { UserModel } from '../models';

export const initialState: RootState.UserState = [
  {
    id: 0,
    name: 'Lukasz Ruminski',
    login: 'hlukarum',
    age: 32,
    verified: true,
  }
];

export const userReducer = handleActions<RootState.UserState, UserModel>(
  {
    [UserActions.Type.ADD_USER]: (state, action) => {
      if (action.payload && action.payload.name) {
        return [
          {
            id: state.reduce((max, user) => Math.max(user.id || 1, max), 0) + 1,
            verified: false,
            name: action.payload.name,
            login: action.payload.login,
            age: action.payload.age,
          },
          ...state
        ];
      } else {
        return state;
      }
    },
    [UserActions.Type.DELETE_USER]: (state, action) => {
      return state.filter((user) =>
        user.id !== (action.payload as any));
    },
    [UserActions.Type.EDIT_USER]: (state, action) => {
      return state.map((user) => {
        if (!user || !action || !action.payload) {
          return user;
        } else {
            return (user.id || 0) === action.payload.id
            ? { ...user,
                name: action.payload.name,
                login: action.payload.login,
            }
            : user;
        }
      });
    },
    [UserActions.Type.VERIFY_USER]: (state, action) => {
  return state.map(
    (user) => (user.id === (action.payload as any) ? { ...user, verified: !user.verified } : user)
  );
},
  },
  initialState
);
