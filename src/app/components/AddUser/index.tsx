import * as React from 'react';
import { UserTextInput } from '../UserTextInput';
import { UserActions } from '../../actions/users';

export namespace AddUser {
  export interface Props {
    addUser: typeof UserActions.addUser;
  }
}

export class AddUser extends React.Component<AddUser.Props> {
  constructor(props: AddUser.Props, context?: any) {
    super(props, context);
  }

  handleSave = (login: string, name: string, age: number) => {
    if (login.length && name.length) {
      this.props.addUser({ login, name, age});
    }
  }

  render() {
    return (
      <header>
        <h1>Users</h1>
        <UserTextInput
          newUser={true}
          onSave={this.handleSave}
          placeholderLogin="Type your login"
          placeholderName="Type yor first and last name"
          placeholderAge="Type your age"
        />
      </header>
    );
  }
}
