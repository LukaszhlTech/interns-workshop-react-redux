import * as React from 'react';
import * as classNames from 'classnames';
import * as style from './style.css';

export namespace UserTextInput {
  export interface Props {
    login?: string;
    name?: string;
    age?: number | undefined;
    placeholderLogin?: string;
    placeholderName?: string;
    placeholderAge?: string;
    newUser?: boolean;
    editing?: boolean;
    onSave: (login: string, name: string, age: number) => void;
  }

  export interface State {
    name: string;
    login: string;
    age: number;
  }
}

export class UserTextInput extends React.Component<UserTextInput.Props, UserTextInput.State> {
  constructor(props: UserTextInput.Props, context?: any) {
    super(props, context);
    this.state = { login: this.props.login || '', name: this.props.name || '', age: this.props.age || 0};
  }

  handleSubmit = (event: React.MouseEvent<HTMLInputElement>) => {
    const {login, name, age} = this.state;
    this.props.onSave(login, name, age);
    if (this.props.newUser) {
      this.setState({ login: '' , name: '', age: 0});
    }
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const eventName = event.target.name;
    if (eventName === 'login') {
      this.setState({ login: event.target.value });
    } else if (eventName === 'name'){
      this.setState({name: event.target.value});
    } else if (eventName === 'age'){
      this.setState({age: event.target.valueAsNumber});
    }
  }

  render() {
    const classes = classNames(
      {
        [style.edit]: this.props.editing,
        [style.new]: this.props.newUser
      },
      style.normal
    );

    return (
      <>
        <input
          className={classes}
          type="text"
          name="login"
          autoFocus
          placeholder={this.props.placeholderLogin}
          value={this.state.login}
          onChange={this.handleChange}
        />
        <input
          className={classes}
          type="text"
          name="name"
          autoFocus
          placeholder={this.props.placeholderName}
          value={this.state.name}
          onChange={this.handleChange}
        />
        <input
          className={classes}
          type="number"
          name="age"
          autoFocus
          placeholder={this.props.placeholderAge}
          value={this.state.age === 0 ? '' : this.state.age}
          onChange={this.handleChange}
        />
        <input
          type="button"
          onClick={this.handleSubmit}
          value={this.props.newUser ? 'Create' : 'Save'}
          />


      </>
    );
  }
}
