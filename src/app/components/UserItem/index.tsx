import * as React from 'react';
import * as classNames from 'classnames';
import * as style from './style.css';
import { UserModel } from '../../models';
import { UserActions } from '../../actions';
import { UserTextInput } from '../UserTextInput';

export namespace UserItem {
  export interface Props {
    user: UserModel;
    editUser: typeof UserActions.editUser;
    deleteUser: typeof UserActions.deleteUser;
    verifyUser: typeof UserActions.verifyUser;
  }

  export interface State {
    editing: boolean;
  }
}

export class UserItem extends React.Component<UserItem.Props, UserItem.State> {
  constructor(props: UserItem.Props, context?: any) {
    super(props, context);
    this.state = { editing: false };
  }

  handleDoubleClick() {
    this.setState({ editing: true });
  }

  handleSave(id: number, login: string, name: string, age: number) {
    if (login.length === 0 || name.length === 0) {
      this.props.deleteUser(id);
    } else {
      this.props.editUser({ id, login, name, age });
    }
    this.setState({ editing: false });
  }

  render() {
    const { user, verifyUser, deleteUser } = this.props;

    let element;
    if (this.state.editing) {
      element = (
        <UserTextInput
          name={user.name}
          login={user.login}
          age={user.age}
          editing={this.state.editing}
          onSave={(login: string, name: string, age: number) => user.id && this.handleSave(user.id, login, name, age)}
        />
      );
    } else {
      element = (
        <div className={style.view}>
          <input
            className={style.toggle}
            type="checkbox"
            checked={user.verified}
            onChange={() => user.id && verifyUser(user.id)}
          />
          <label onDoubleClick={() => this.handleDoubleClick()}>{user.name} <em>{user.login}</em> age: <em>{user.age}</em></label>
          <button
            className={style.destroy}
            onClick={() => {
              if (user.id) deleteUser(user.id);
            }}
          />
        </div>
      );
    }

    // TODO: compose
    const classes = classNames({
      [style.verified]: user.verified,
      [style.editing]: this.state.editing,
      [style.normal]: !this.state.editing,
    });
    console.log(classes);

    return <li className={classes}>{element}</li>;
  }
}
