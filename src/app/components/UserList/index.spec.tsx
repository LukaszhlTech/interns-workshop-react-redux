import * as React from 'react';
import { shallow } from 'enzyme';

import UserList from './index';
import { AddUser } from '../AddUser';
import { UserItem } from '../UserItem';

import { UserActions as actions } from '../../actions/users';
import { initialState as users } from '../../reducers/users';

const pretty = require('pretty');
const props = {actions, users};

describe('Render UserList', () => {
  it('should render UserList Component', () => {
    const renderedUserList: any = shallow(<UserList {...props}  />).containsAllMatchingElements([
      <AddUser/>,
      <section>
        <ul>
          <UserItem/>
        </ul>
      </section>
    ]);
    const renderedUserListContains: any = shallow(<UserList {...props}  />);
    console.log(
      pretty(renderedUserListContains.html(), { indent_size: 2 })
    );
    expect(renderedUserList).toBeTruthy();
    expect(renderedUserListContains).toBeTruthy();

  })
});
