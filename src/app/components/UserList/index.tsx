import * as React from 'react';
import * as style from './style.css';
import { UserActions } from '../../actions/users';
import { UserItem } from '../UserItem';
import { UserModel } from '../../models/UserModel';
import { AddUser } from '../AddUser';

export namespace UserList {
  export interface Props {
    users: UserModel[];
    actions: UserActions;
  }
}

class UserList extends React.Component<UserList.Props> {

  render(): React.ReactNode {
    const { users, actions } = this.props;
    return (
      <>
        <AddUser addUser={actions.addUser}/>
        <section className={style.main}>
          <ul className={style.normal}>
            {users.map((user) => (
              <UserItem
                key={user.id}
                user={user}
                verifyUser={actions.verifyUser}
                deleteUser={actions.deleteUser}
                editUser={actions.editUser}
              />
            ))}
          </ul>
        </section>
      </>
    );
  }
}

export default UserList;
